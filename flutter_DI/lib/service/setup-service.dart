import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'setup-service.config.dart';

GetIt getIt = GetIt.instance;

// @InjectableInit()
// Future<void> setupService() {
//   // getIt.registerLazySingleton<Service1>(() => Service1());
//   return $initGetIt(getIt);
// }

@InjectableInit()
Future<void> setupService() async => $initGetIt(getIt);

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_canvas/screen/canvas1.dart';
import 'package:flutter_canvas/service/service1.dart';
import 'package:flutter_canvas/service/setup-service.dart';

void main() {
  setupService();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> btns = [
      Container(
        decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5.0))),
        child: TextButton(
          onPressed: () {
            // Service1 service1 = getIt.get<Service1>();
            // service1.getInfor();
            Navigator.push(
                context, MaterialPageRoute(builder: (c) => const Canvas1()));
          },
          child: const Text("Btn 1"),
        ),
      ),
      Container(
        decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5.0))),
        child: TextButton(
          onPressed: () {
            Service1 service1 = getIt.get<Service1>();
            service1.getInfor();
          },
          child: const Text("Btn 2"),
        ),
      ),
    ];
    return Scaffold(
      appBar: AppBar(
        title: const Text("Canvas"),
      ),
      body: ListView.builder(
          itemCount: btns.length,
          itemBuilder: (context, index) {
            return btns[index];
          }),
    );
  }
}

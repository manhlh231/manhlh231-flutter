import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_canvas/screen/custom-canvas.dart';
import 'package:flutter_canvas/service/service1.dart';
import 'package:get_it/get_it.dart';

class Canvas1 extends StatefulWidget {
  const Canvas1({Key? key}) : super(key: key);

  @override
  State<Canvas1> createState() => _Canvas1State();
}

class _Canvas1State extends State<Canvas1> {
  MyFancyPainter custom = MyFancyPainter();
  GetIt getIt = GetIt.I;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("title"),
      ),
      body: TextButton(
          onPressed: () {
            var service = getIt.get<Service1>();
            service.getInfor();
          },
          child: const Text("Btn")),
    );
  }
}
